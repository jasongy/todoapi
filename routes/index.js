var express = require('express');
var router = express.Router();
var cors = require('cors');
// /* GET home page. */
// router.get('/', function(req, res, next) {
//   res.render('index', { title: 'Express' });
// });

var mongoose = require('mongoose');
var db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log("Connection to database established");
});

var taskSchema = mongoose.Schema({
  task: String,
  status: Boolean,
  due: String
});

var Task = mongoose.model('Task', taskSchema);
// The first parameter is the name of the collection

router.patch('/:id', cors(), function(req, res) {
  Task.findById(req.params.id, function(err, doc) {
    if (err) throw err;
    doc.task = req.body.task;
    doc.status = req.body.status;
    doc.due = req.body.due;

    doc.save(function() {
      if (err) throw err;
      res.status(201).json({data: doc});
    });
  });
});

router.options('/', cors());

router.patch('/', cors(), function(req, res) {
  var ids = req.body['ids[]'];
  for (var i = 0; i < ids.length; i++) {
    Task.findById(ids[i], function(err, doc) {
      if (err) throw err;
      doc.status = !doc.status;
      
      doc.save(function() {
        if (err) throw err;
        res.status(201).send();
      });
    });
  }
});

router.options("/:id", cors());

router.delete('/:id', cors(), function(req, res) {
  Task.remove({_id: req.params.id}, function (err) {
    if (err) throw err;
    res.status(204).send();
  })
})

router.get('/', cors(), function(req, res, next) {
  Task.find(function (err, docs) {
    if (err) throw err;
    res.status(200).json({data: docs});
  });
});

router.post('/', cors(), function(req, res) {
  var newTask = new Task(req.body);
  newTask.save(function(err, doc) {
    if (err) throw err;
    res.status(201).json({data: doc});
  })
});

module.exports = router;
